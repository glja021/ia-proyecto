%   @author glj
%   @version 1	2016-06-01
%   @desc aplicacion del proyecto final de IA
%

%% PARTE 1
cd 'src'
% Set de datos
data = importfile('../data/bank-additional-full.csv'); 
% set de datos de prueba
%data = importfile('../data/bank-additional.csv'); 

% Particionamiento
[idxTrain, idxTest, ~] = dividerand(41188,0.7,0.3,0);
% prueba 
%[idxTrain, idxTest, ~] = dividerand(4119,0.7,0.3,0);
training = data(idxTrain,:);
testing = data(idxTest,:);

% Normalizacion 
normalizacion = true;
% Seleccion de caracteristicas y normalizacion dentro de la funcion trainClassifier

%% PARTE 2
% Set de training 
% Generar el modelo con el mejor % de exactitud
[modelosvm, exactitud] = trainClassifier(training,'gaussian',normalizacion);
% Por lo general tiene uno o dos % menos el kernel lineal
%[modelosvm, exactitud] = trainClassifier(training,'linear',normalizacion);

%% PARTE 3
% Predecir la clase del set Test con el modelo entrenado
predicted = modelosvm.predict(testing);
% obtener el % de exactitud ~~~~
disp('exactitud del modelo svm esperada por el modelo: ');
disp(exactitud*100);
i=1;
miss = 0;
sz = length(predicted);
while i<=sz
	if(predicted(i) ~= categorical(testing.y(i)))
		miss = miss + 1;
	end % end if
	i = i +1;
end %end while
disp('exactitud del modelo svm probada contra matriz de test: ');
disp(100-(miss / sz * 100));

%%%% CREOO QUE ES TODO
cd ..