inputTable = data;
predictorNames = {'age', 'job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'day_of_week', 'duration', 'campaign', 'pdays', 'previous', 'poutcome', 'empvarrate', 'conspriceidx', 'consconfidx', 'euribor3m', 'nremployed'};
predictors = inputTable(:, predictorNames);
response = inputTable.y;
isCategoricalPredictor = [false, true, true, true, true, true, true, true, true, true, false, false, false, false, true, false, false, false, false, false];
numericPredictors = predictors(:, ~isCategoricalPredictor);
cd 'src'
%numericPredictors = table2array(varfun(@double, numericPredictors));
%%%%% MINMAX
numericPredictors = min_max(table2array(varfun(@double, numericPredictors)));
%%%%%%%%%%%%% PCA .9
%numericPredictors(isinf(numericPredictors)) = NaN;
%[pcaCoefficients, pcaScores, ~, ~, explained, pcaCenters] = pca(numericPredictors);
%explainedVarianceToKeepAsFraction = 9800000/10000000;
%numComponentsToKeep = find(cumsum(explained)/sum(explained) >= explainedVarianceToKeepAsFraction, 1);
%pcaCoefficients = pcaCoefficients(:,1:numComponentsToKeep);
%numericPredictors = pcaScores(:,1:numComponentsToKeep);

%%%%%%%%%%%%
cd 'visualizacion'
%visualizacion(numericPredictors(idxTrain,:), data.y(idxTrain))
visualizacion(numericPredictors(idxTest,:), predicted)
cd '../..'