# Reporte de investigación
## Investigacion del set de datos
 El set de datos que se selecciono contiene informacion acerca de una campaña de marketing realizada via telefonica en una institucion bancaria de origen portugues

 Contiene dos clases ubicadas en la columna `y` con valores {yes, no} en donde el resultado depende de si el cliente se suscribio a un deposito bancario

 Fue obtenido a partir de este [vinculo](https://archive.ics.uci.edu/ml/datasets/Bank+Marketing) en el sitio de UCI

 El set de datos contiene 20 atributos en total, ademas del atributo de clase los cuales vienen marcados en la primera fila por medio de encabezados, son los siguientes:

	+ age (numeric): la edad
	+ job (cat): el giro del trabajo 
	+ marital (cat): estado civil
	+ education(cat): el grado de estudios
	+ default(cat): si posee una linea de credito
	+ housing(cat): si posee una hipoteca
	+ loan(cat): si posee un prestamo personal
	+ contact(cat): si se contacto via telefono o celular
	+ month(cat):  el mes del año donde se produjo el ultimo contacto con la campaña
	+ day_of_week(cat): el dia de la semana donde se produjo el ultimo contacto
	+ duration(numeric): la duracion de el ultimo contacto, en segundos
	+ campaign(numeric): el numero de contactos que se tuvo con el cliente
	+ pdays(numeric): el numero de dias transcurridos desde el ultimo contacto, si se tuvo mas de uno
	+ previous(numeric): el numero de contactos previos antes de esta campaña
	+ poutcome(cat): el resultado de la campaña anterior en dicho cliente
	+ emp.var.rate(numeric): indicador de la variacion en el empleo
	+ cons.conf.idx(numeric): el indice de confianza en el consumidor
	+ euribor3m(numeric): indicador euribor a 3 meses 
	+ nr.employed(numeric): el numero de empleados
> **Referencias:** S. Moro, P. Cortez and P. Rita. A Data-Driven Approach to Predict the Success of Bank Telemarketing. Decision Support Systems, Elsevier, 62:22-31, June 2014

>S. Moro, R. Laureano and P. Cortez. Using Data Mining for Bank Direct Marketing: An Application of the CRISP-DM Methodology. In P. Novais et al. (Eds.), Proceedings of the European Simulation and Modelling Conference - ESM'2011, pp. 117-121, Guimaraes, Portugal, October, 2011. EUROSIS. [bank.zip]


## Visualizaciones de cada etapa en el set de datos
Las siguientes graficas fueron generadas mediante el script graficar, en distintos puntos dentro del proceso de clasificacion, para propositos de visualizacion se utilizo el set con 4119 muestras
> Set completo original
![](../img/set-original.png "Set original")

> Set de training, 70% del original
![](../img/set-train-original.png "Set training")

> Set de training, despues de seleccion de atributos mediante PCA con una varianza de 95%
![](../img/set-train-pca.png "PCA")

> Set de training, despues de normalizacion usando min max de 0-1
![](../img/set-train-minmax.png "Min/Max")


> Set de test, con la clase predecida por el modelo SVM entrenado
![](../img/set-test-predecido-svm.png "Set test")

> Set de test, con la clase predecida por el modelo SVM entrenado en el set original (~41000 muestras)
![](../img/set-test-completo.png "Resultado final")

## Mejores parametros de entrenamiento

Los parametros que se seleccionaron para entrenar el modelo de SVM fueron los siguientes:
	
	+ KernelFunction : Gaussian
	+ KernelScale : 21.803
	+ BoxConstraint : 1
	+ Standardize : 1
	+ PCA variance : 0.9
	+ Rango min/max : 0 - 1 

Se realizaron pruebas con distintos valores, entre ellos la utilizacion de un kernel lineal en vez de gaussiano y los parametros seleccionados fueron los que obtuvieron mejores resultados, con un porcentaje de exactitud entre 1 y 3% mas alto que los demas	en promedio

## Conclusiones acerca del algoritmo

Con los parametros seleccionados se obtuvo un porcentaje de exactitud de entre 89 y 91 %, el set de datos se redujo a 12 de las 20 dimensiones originales que fueron las que resultaro relevantes segun el algoritmo PCA. Se probaron los diferentes tipos de kernel en el modelo SVM y mediante el descarte de los kernels con menor grado de exactitud se selecciono la opcion final.
Los registros en el set de datos son en su mayor medida clasificables y ello contribuye al alto porcentaje de exito.
