# Proyecto IA
# [Reporte de investigacion](docs/reporte.md) <~

## 1- Preprocesamiento
	+ set de datos
	+ particionamiento
		+ 70% training
		+ 30% test
	+ Normalizacion
	+ reduccion de dimencionalidad
		+ SFS o PCA

## 2- Entrenamiento
	+ set de training (reducido, normalizado)
	+ SVM o NN
	+ Obtener mejor % de exactitud (CV)
	+ Generar el modelo entrenado


## 3- Prueba
	+ Set de test
	+ Predecir el set de test con el modelo entrenado
	+ Obtener % de exactituda de test


## 4- Reporte de investigación
	+ Investigacion de set de datos
	+ Incluir visualizaciones de cada etapa (Cambio) del set de datos
	+ incluir mejores parametros de entrenamiento
	+ Dependiendo de su % de exactitud final, intentar "explicar" porque el algoritmo dio ese resultado


# [Reporte de investigacion](docs/reporte.md) <~

**Ordinario: Lunes 5 de junio 6pm.**

**Entrega de Proyecto: Viernes 2 de junio 6pm.**