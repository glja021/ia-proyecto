function [trainedClassifier, validationAccuracy] = trainClassifier(trainingData,kernel,normalization)
%   @author glj
%   @version 1  2016-06-01
%   @desc funcion para entrenar un modelo de clasificacion SVM a partir del set de datos bank
%   @params 'trainingData', table Nx21 > los datos de training para el set bank
%   @params 'kernel', string > el tipo del kernel para svm
%   @params 'normalization' bool > flag para fitcsvm en caso de no querer normalizar las variables categoricas
%									 no la quites csm tarda mil anyos
%   @returns 'trainedClassifier' > estructura con el modelo svm, las variables de pca y la funcion para clasificar nuevas tablas
%   @returns 'validationAccuracy' > la exactitud esperada segun el calculo sobre el modelo train
%
inputTable = trainingData;
predictorNames = {'age', 'job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'day_of_week', 'duration', 'campaign', 'pdays', 'previous', 'poutcome', 'empvarrate', 'conspriceidx', 'consconfidx', 'euribor3m', 'nremployed'};
predictors = inputTable(:, predictorNames);
response = inputTable.y;
isCategoricalPredictor = [false, true, true, true, true, true, true, true, true, true, false, false, false, false, true, false, false, false, false, false];
isCategoricalPredictorBeforePCA = isCategoricalPredictor;
numericPredictors = predictors(:, ~isCategoricalPredictor);

%% Normaliza usando min_max
if(normalization == true)
    numericPredictors = min_max(table2array(varfun(@double, numericPredictors)));   
else
    numericPredictors =table2array(varfun(@double, numericPredictors));    
end

%% Reduce el set de datos usando PCA con una varianza de .9
numericPredictors(isinf(numericPredictors)) = NaN;
[pcaCoefficients, pcaScores, ~, ~, explained, pcaCenters] = pca(...
    numericPredictors);
explainedVarianceToKeepAsFraction = 90/100;
numComponentsToKeep = find(cumsum(explained)/sum(explained) >= explainedVarianceToKeepAsFraction, 1);
pcaCoefficients = pcaCoefficients(:,1:numComponentsToKeep);
predictors = [array2table(pcaScores(:,1:numComponentsToKeep)), predictors(:, isCategoricalPredictor)];
isCategoricalPredictor = [false(1,numComponentsToKeep), true(1,sum(isCategoricalPredictor))];

%% usa la funcion fitcsvm para entrenar un modelo de SVM
%%%%%%%%%%%%%%%%%%%%%%% 'KernelScale', 'auto', ...
  classificationSVM = fitcsvm(...
    predictors, ...
    response, ...
    'KernelFunction', kernel, ...
    'PolynomialOrder', [], ...
    'KernelScale', 'auto', ...
    'BoxConstraint', 1, ...
    'Standardize', normalization, ...
    'ClassNames', categorical({'no'; 'yes'}));
%%%%%%%%%%%%%%%%%%%%%%%%

%% extrae la funcion para clasificar a partir del modelo SVM
predictorExtractionFcn = @(t) t(:, predictorNames);
pcaTransformationFcn = @(x) [ array2table((table2array(varfun(@double, x(:, ~isCategoricalPredictorBeforePCA))) - pcaCenters) * pcaCoefficients), x(:,isCategoricalPredictorBeforePCA) ];
svmPredictFcn = @(x) predict(classificationSVM, x);

%% asigna los atributos del objeto trainedClassifier
trainedClassifier.predict = @(x) svmPredictFcn(pcaTransformationFcn(predictorExtractionFcn(x)));
trainedClassifier.RequiredVariables = {'age', 'job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'day_of_week', 'duration', 'campaign', 'pdays', 'previous', 'poutcome', 'empvarrate', 'conspriceidx', 'consconfidx', 'euribor3m', 'nremployed'};
trainedClassifier.PCACenters = pcaCenters;
trainedClassifier.PCACoefficients = pcaCoefficients;
trainedClassifier.ClassificationSVM = classificationSVM;

%% Obtiene el porcentaje de exactitud esperado segun las pruebas en el set de training
inputTable = trainingData;
predictorNames = {'age', 'job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'month', 'day_of_week', 'duration', 'campaign', 'pdays', 'previous', 'poutcome', 'empvarrate', 'conspriceidx', 'consconfidx', 'euribor3m', 'nremployed'};
predictors = inputTable(:, predictorNames);
response = inputTable.y;
isCategoricalPredictor = [false, true, true, true, true, true, true, true, true, true, false, false, false, false, true, false, false, false, false, false];
validationPredictFcn = @(x) svmPredictFcn(pcaTransformationFcn(x));
[validationPredictions, validationScores] = validationPredictFcn(predictors);
correctPredictions = (validationPredictions == response);
isMissing = ismissing(response);
correctPredictions = correctPredictions(~isMissing);
validationAccuracy = sum(correctPredictions)/length(correctPredictions);
