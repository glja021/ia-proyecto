function M = min_max(matrix)
%   @author glj
%   @version 2	2016-06-01
%   @desc funcion para normalizar los atributos de una matriz  mediante 
%   	el algoritmo min max con rango de 0 - 1
%	@params 'matrix' > una matriz con los datos a normalizar
%			[101 201 301 ; 101 202 303 200 ; 141 231 341; 151 232 311 ;  
%			101 201 301 ; 101 233 311 ; 144 211 311 ; 146 231 303 ]
%	@returns 'M' > la misma matriz con los atributos normalizados
%
	attr = 1;
	msize = size(matrix,2);
	while(attr<msize)
		matrix(:,attr) = normalizeAColumn(matrix(:,attr));
		attr = attr +1;
	end %end of while loop
	M = matrix;
end % end of min_max function

function c = normalizeAColumn(column)
%   @author glj
%   @version 1	2016-03-22
%   @desc funcion para normalizar los atributos de una columna de 0-1 
%	@params 'column' > una columna con n atributos
%	@returns 'c' > la misma columna normalizada
%
	low = min(column);
	high = max(column);
	%% formula de clase para normalizar vvvv
	%% v' = (nMaxA-nMinA)[(v-minA)/(maxA-minA)] + nMinA
	% c = (1-0) [(column-low)./(high-low)] + 0 
  c = (column-low)./(high-low);	
end %end of normalizeAColumn function
