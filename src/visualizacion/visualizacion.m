function visualizacion( X, Y )
%Función de Visualización
%X = Datos a Visualizar
%Y = Etiquetas de datos a Visualizar
    marcadores = {'o' 's' 'd' 'h'};
    colores = {'red' 'blue' 'green' 'magenta'};
    muestras = X;
    etiquetas = Y;
    
    vclases = unique(etiquetas);
    
    figure1 = figure;
    axes1 = axes('Parent',figure1);
    hold(axes1,'all');
    
    data_pca = proypca(muestras,n);%3 Dimensiones
    
    for i=1:length(vclases)
        indices = find(etiquetas == vclases(i));
        scatter3(data_pca(indices,1),data_pca(indices,2),data_pca(indices,3),...
        marcadores{i},'MarkerEdgeColor','black','MarkerFaceColor',colores{i});
        
        clear z1 z2 z3 z4;
        legend('-1','+1');
    end
end

