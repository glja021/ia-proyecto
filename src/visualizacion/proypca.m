function signals=proypca(data,l)
%PCA: perform PCA using covariance
%data - MxM matris od input data
% (M trials, N dimension)
% sinfals - MxM matrix of projected data
% PC - each column is a PC

[M,~] = size(data);

%subtract off ther mean for each dimension
mn = mean(data,1);
data = data - repmat(mn, M,1);

%subtract off the standar deviarion for each dimension
%stdr = std(data);
%data = data./repmat(stdr,M,1);

%calculate the variance matrix
covariance = cov(data);

%find the eigenvectors and eigenvalues
[PC, V] = eig(covariance);

% extract diagonal of matrix as vector
V = diag(V);

%sort the variances in decreasing order
[~,rindices] = sort(-1*V);
%V = V(rindices);
PC = PC(:,rindices);

%project the original data set
signals = data*PC(:,1:l);

end